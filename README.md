# WeatherApp
> A simple iOS application that shows current temperature and weather condition for a searched city or from current location. 

## Screenshots
![Dark appearance](WeatherApp/Dark%20Appearance.png)
![Light appearance](WeatherApp/Light%20Appearance.png)

## Technologies
* Objective - C

## Features
* REST API
* Delegate Pattern
* MVC Design Pattern
* Supportable for both Dark and Light appearence in an iOS app.

## Contact
Created by [@vikihacksys](https://gitlab.com/vikihacksys) - feel free to contact me!
